<?php

define('APP_ENV', 'dev');

define('PUBLIC_PATH', realpath('..') . '/public/');

define('CONFIGS_PATH', APP_PATH . 'config/configs/');

define('VENDOR_PATH', realpath('..') . '/vendor/');