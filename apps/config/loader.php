<?php
use Phalcon\Debug;
use Phalcon\Loader;

if (APP_ENV === 'dev') {
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
    ini_set('display_errors', 1);

    $debug = new Debug();
    $debug->listen();
} else {
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
    ini_set('display_errors', 0);
}

mb_internal_encoding('UTF-8');
date_default_timezone_set('UTC');

$loader = new Loader();

//Registers namespaces for all modules
$loader->registerNamespaces([
        'Loltome\Models\Entities' => APP_PATH . 'models/entities/',
        'Loltome\Models\Services' => APP_PATH . 'models/services/',
        'Loltome\Models\Repositories' => APP_PATH . 'models/repositories/',
        'Loltome\Models\Listeners' => APP_PATH . 'models/listeners/',
        'Loltome\Models\Normalizers' => APP_PATH . 'models/normalizers/',
        'Loltome\Plugins' => APP_PATH . 'library/plugins/',
        'Loltome\DTO' => APP_PATH . 'library/dto/',
        'Loltome\Helpers' => APP_PATH . 'library/helpers/',
        'Loltome\Traits' => APP_PATH . 'library/traits/',
        'Modules\Application\Controllers' => APP_PATH . 'modules/application/controllers/',
        'Modules\Application\Forms' => APP_PATH . 'modules/application/forms/'
    ]);

$loader->register();

require_once VENDOR_PATH . 'autoload.php';