<?php

/**
 * Register application modules
 */
$application->registerModules(
    [
        'application'  => [
            'className' => 'Modules\Modules\Application\Module',
            'path'      => __DIR__ . '/../modules/application/Module.php'
        ],
        'dashboard' => [
            'className' => 'Modules\Modules\Dashboard\Module',
            'path'      => __DIR__ . '/../modules/dashboard/Module.php'
        ]
    ]
);
