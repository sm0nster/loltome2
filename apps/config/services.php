<?php
use Phalcon\Config\Adapter\Ini as ConfigIni;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\DI\FactoryDefault;
use Phalcon\Events\Manager;
use Phalcon\Logger;
use Phalcon\Mvc\Router\Annotations as RouterAnnotations;
use Phalcon\Mvc\Url;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Session\Adapter\Files as SessionAdapter;

$di = new FactoryDefault();

$di->setShared('router', function () {

    $router = new RouterAnnotations(false);
    $router->addModuleResource('application', 'Modules\Application\Controllers\Index');
    $router->addModuleResource('application', 'Modules\Application\Controllers\Error');
    $router->addModuleResource('application', 'Modules\Application\Controllers\Landing');
    $router->addModuleResource('application', 'Modules\Application\Controllers\Notworking\Notworking');

    $router->setDefaultModule('application');
    $router->setDefaultNamespace('Modules\Application\Controllers');
    $router->setDefaultController('index');
    $router->setDefaultAction('index');
    $router->removeExtraSlashes(true);
    $router->notFound(
        [
            'namespace' => 'Modules\Application\Controllers',
            'controller' => 'error',
            'action' => 'show404'
        ]);
    $router->setUriSource(\Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

    return $router;
});

$di->setShared('url', function () {
    $url = new Url();
    $url->setBaseUri($this->get('config')->application->baseUrl);
    $url->setStaticBaseUri($this->get('config')->application->staticUrl);
    return $url;
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();
    return $session;
});

$di->setShared('config', function () {
    /** @var \Phalcon\Config $config */
    $config = new ConfigIni(CONFIGS_PATH . 'config.ini');

    if(APP_ENV === 'dev'){
        $extendedConfig = new ConfigIni(CONFIGS_PATH . 'config_dev.ini');
        $config->merge($extendedConfig);
    }
    return $config;
});

$di->setShared('db', function () {
    $connection = new DbAdapter([
        'host' => $this->get('config')->database->host,
        'username' => $this->get('config')->database->username,
        'password' => $this->get('config')->database->password,
        'dbname' => $this->get('config')->database->dbname,
        'charset' => 'utf8',
        'dialectClass' => '\Phalcon\Db\Dialect\MysqlExtended',
        'options' => [
            PDO::ATTR_PERSISTENT => $this->get('config')->database->persistent,
            PDO::ATTR_EMULATE_PREPARES => false,
        ]
    ]);

    //Events logger
    $logger = new Phalcon\Logger\Adapter\File(APP_PATH . 'logs/mysql.log');
    $eventsManager = new Manager();
    $eventsManager->attach('db', function ($event, $connection) use ($logger) {
        if ($event->getType() == 'beforeQuery') {
            $logger->log($connection->getRealSQLStatement(), Logger::INFO);
        }
    });
    $connection->setEventsManager($eventsManager);

    return $connection;
});

$di->setShared('logger', function () {
    $logger = new \Phalcon\Logger\Multiple();

    $fileLogger = new Phalcon\Logger\Adapter\File\Multiple(APP_PATH . 'logs/' . APP_ENV);

    $logger->push($fileLogger);
    return $logger;
});

//TODO after auth plugin
//$di->setShared('acl', new Acl());
//
//$di->set('dispatcher', function () {
//    $eventsManager = new Manager();
//
//    $eventsManager->attach('dispatch:beforeDispatch', $this->get('acl'));
//
////TODO handling exceptions and not-found exceptions
////    //Handle exceptions and not-found exceptions using ExceptionPlugin
////    $eventsManager->attach('dispatch:beforeException', new ExceptionPlugin());
//
//    $dispatcher = new Dispatcher;
//    $dispatcher->setEventsManager($eventsManager);
//    $dispatcher->setDefaultNamespace('Modules\Application\Controllers');
//    return $dispatcher;
//});

$di->setShared('annotations', function () {
    if ($this->get('config')->cache->use == 'true') {
        $lifetime = 86400;
    } else {
        $lifetime = 1;
    }

    return new \Phalcon\Annotations\Adapter\Memcached([
        'host' => $this->get('config')->memcached->host,
        'port' => $this->get('config')->memcached->port,
        'weight' => $this->get('config')->memcached->weight,
        'lifetime' => $lifetime
    ]);
});