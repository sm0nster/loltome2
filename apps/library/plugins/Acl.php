<?php
namespace Loltome\Plugins;

use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Exception;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl as PhalconAcl;
use Phalcon\Acl\Role;

class Acl extends Plugin
{
    const CACHE_KEY = 'internal_acl_list';

    /**
     * The ACL Object
     *
     * @var \Phalcon\Acl\Adapter\Memory
     */
    private $acl;

    private $adminResources = [
        'application' => [
            'index' => ['index3'],
        ]
    ];
    private $userResources = [
        'application' => [
            'index' => ['index2'],
        ]
    ];
    private $publicResources = [
        'application' => [
            'index' => ['index'],
            'error' => ['show404']
        ]
    ];
    private $roles = [
        'admin' => null,
        'user' => null,
        'guest' => null
    ];

    private function createResources(AclList $aclList)
    {
        foreach ($this->publicResources as $moduleName => $controllers) {
            foreach ($controllers as $controllerName => $actions) {
                $aclList->addResource(new Resource($this->mergeResource($moduleName, $controllerName)), $actions);
            }
        }

        foreach ($this->userResources as $moduleName => $controllers) {
            foreach ($controllers as $controllerName => $actions) {
                $aclList->addResource(new Resource($this->mergeResource($moduleName, $controllerName)), $actions);
            }
        }

        foreach ($this->adminResources as $moduleName => $controllers) {
            foreach ($controllers as $controllerName => $actions) {
                $aclList->addResource(new Resource($this->mergeResource($moduleName, $controllerName)), $actions);
            }
        }
    }

    private function buildAcl()
    {
        $aclList = new AclList();

        $aclList->setDefaultAction(PhalconAcl::DENY);

        //Create roles
        foreach ($this->roles as $key => &$value) {
            $value = new Role($key);
            $aclList->addRole($value);
        }

        //Create resources
        $this->createResources($aclList);

        //Grant access to public areas to EACH role
        foreach ($this->publicResources as $moduleName => $controllers) {
            foreach ($controllers as $controllerName => $actions) {
                foreach ($actions as $action) {
                    foreach ($this->roles as $role) {
                        $aclList->allow($role->getName(), $this->mergeResource($moduleName, $controllerName), $action);
                    }
                }
            }
        }

        //Grant access to user area to role User
        foreach ($this->userResources as $moduleName => $controllers) {
            foreach ($controllers as $controllerName => $actions) {
                foreach ($actions as $action) {
                    $aclList->allow('User', $this->mergeResource($moduleName, $controllerName), $action);
                }
            }
        }

        //Grant access to admin area to role Admin
        foreach ($this->adminResources as $moduleName => $controllers) {
            foreach ($controllers as $controllerName => $actions) {
                foreach ($actions as $action) {
                    $aclList->allow('Admin', $this->mergeResource($moduleName, $controllerName), $action);
                }
            }
        }

//TODO add cache
//        $this->memcached->save(self::CACHE_KEY, $aclList);

        return $aclList;
    }

    /**
     * Returns an existing or new access control list
     *
     * @returns AclList
     */
    public function getAcl($useCache = true)
    {
//TODO add cache
//        if ($this->config->cache->use === 'false') {
//            $useCache = false;
//        }
//
//        $useCache = true;

        if (is_object($this->acl)) {
            return $this->acl;
        }

//TODO add cache
//        if ($useCache && $aclCache = $this->memcached->get(self::CACHE_KEY)) {
//            if (is_object($aclCache)) {
//                $this->acl = $aclCache;
//                return $aclCache;
//            }
//        }

        return $this->acl = $this->buildAcl();
    }

//TODO add beforeDispatch after auth plugin
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {

    }

    public function isAllowed($role, $resourceName, $action)
    {
        return $this->getAcl()->isAllowed($role, $resourceName, $action);
    }

    public function mergeResource($moduleName, $controllerName)
    {
        if (mb_strlen($controllerName) === 0) {
            throw new Exception('Empty controller name');
        }

        if (mb_strlen($moduleName) === 0) {
            throw new Exception('Empty module name');
        }

        $nsRes = $moduleName . ':' . $controllerName;
        return $nsRes;
    }
}