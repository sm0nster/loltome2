<?php
namespace FitCompany\Plugins\Auth;

use Loltome\Models\Entities\User\RememberMe;
use Loltome\Models\Entities\User\User;
use Loltome\Plugins\Auth\Login\LoginInterface;
use Loltome\Plugins\Auth\Session\SessionInterface;
use Loltome\Plugins\Auth\Session\UserSession;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\User\Plugin;

class Auth extends Plugin
{
    protected $cacheData = [];

    /** @var  UserSession */
    protected $userSession;

    public function __construct()
    {
        $this->userSession = new UserSession();
    }

    public function login(LoginInterface $loginHandler, $data)
    {
        return $loginHandler->login($data);
    }

    public function loginByRememberMe(LoginInterface $loginHandler, RememberMe $rememberMe)
    {
        return $loginHandler->loginByRememberMe($rememberMe);
    }

    public function logout(SessionInterface $sessionHandler)
    {
        return $sessionHandler->logout();
    }

    /**
     * @param SessionInterface|null $sessionHandler
     * @return User
     */
    public function getUser(SessionInterface $sessionHandler = null)
    {
        $moduleName = $this->dispatcher->getModuleName();

        if (isset($this->cacheData['user']) && $this->cacheData['user']) {
            return $this->cacheData['user'];
        }

        if ($sessionHandler === null && $moduleName === 'admin') {
            $sessionHandler = new AdminSession();
        } elseif ($sessionHandler === null) {
            $sessionHandler = $this->userSession;
        }

        return $this->cacheData['user'] = $sessionHandler->getUser();
    }

    public function getSession(SessionInterface $sessionHandler = null)
    {
        $moduleName = $this->dispatcher->getModuleName();

        if ($sessionHandler === null && $moduleName === 'admin') {
            $sessionHandler = new AdminSession();
        } elseif ($sessionHandler === null) {
            $sessionHandler = $this->userSession;
        }

        return $sessionHandler->getSession();
    }

    public function isLogged(SessionInterface $sessionHandler = null)
    {
        $moduleName = $this->dispatcher->getModuleName();

        if ($sessionHandler === null && $moduleName === 'admin') {
            $sessionHandler = new AdminSession();
        } elseif ($sessionHandler === null) {
            $sessionHandler = $this->userSession;
        }

        return $sessionHandler->isLogged();
    }

    public function updateSession($name, $value, $sessionHandler = null)
    {
        $moduleName = $this->dispatcher->getModuleName();

        if ($sessionHandler === null && $moduleName === 'admin') {
            $sessionHandler = new AdminSession();
        } elseif ($sessionHandler === null) {
            $sessionHandler = $this->userSession;
        }

        return $sessionHandler->updateSession($name, $value);
    }

    /**
     * @return UserSession
     */
    public function getUserSession()
    {
        return $this->userSession;
    }

    public function getHashedSessionId()
    {
        return $this->userSession->getHashedSessionId();
    }

}