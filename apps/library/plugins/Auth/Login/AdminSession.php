<?php
namespace Loltome\Plugins\Auth\Session;


use Loltome\Models\Entities\Admin\Admin;
use Loltome\Models\Entities\Admin\Role;
use Phalcon\Exception;
use Phalcon\Mvc\User\Plugin;

class AdminSession extends Plugin implements SessionInterface
{
    protected $sessionName;
    protected $dataCache = [];

    public function __construct()
    {
        $this->sessionName = $this->di->get('config')->auth->sessionName->admin;

        if ($this->getSession() == null) {
            $this->createSession();
        }
    }

    public function getUser()
    {
        $session = $this->getSession();

        if (isset($session['id'])) {

            if (isset($this->dataCache['admin']) && $this->dataCache['admin']) {
                return $this->dataCache['admin'];
            }

            $admin = $this->dataCache['admin'] = Admin::findFirstById($session['id']);
            if ($admin == false) {
                throw new Exception('The admin does not exist');
            }

            return $admin;
        }

        return false;
    }

    public function getSession()
    {
        if (isset($this->dataCache['session']) && $this->dataCache['session']) {
            return $this->dataCache['session'];
        }

        return $this->dataCache['session'] = $this->session->get($this->sessionName);
    }

    public function createSession(Admin $admin = null)
    {
        if ($admin === null) {
            $this->session->set($this->sessionName, [
                'role' => 'Unlogged'
            ]);
        } else {
            $this->session->set($this->sessionName, [
                'id' => $admin->getId(),
                'username' => $admin->getUsername(),
                'role' => $admin->role->getName()
            ]);
        }
    }

    public function isLogged()
    {
        /** @var Role $role */
        $role = Role::findFirstByName($this->getSession()['role']);

        if ($role && $role->isLoggable()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSession($name, $value)
    {
        $session = $this->getSession();
        $session[$name] = $value;
        $this->session->set($this->sessionName, $session);
    }

    public function logout()
    {
        $this->session->remove($this->sessionName);
    }
}