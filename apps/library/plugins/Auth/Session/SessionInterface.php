<?php
namespace Loltome\Plugins\Auth\Session;

interface SessionInterface
{
    public function getUser();

    public function getSession();

    public function createSession();

    public function logout();
}
