<?php
namespace Loltome\Plugins\Auth\Session;

use Loltome\Models\Entities\User\User;
use Loltome\Models\Services\Services;
use Phalcon\Exception;
use Phalcon\Mvc\User\Plugin;

class UserSession extends Plugin implements SessionInterface
{
    protected $sessionName;
    protected $dataCache = [];

    public function __construct()
    {
        $this->sessionName = $this->di->get('config')->auth->sessionName->user;

        if ($this->getSession() == null) {
            $this->createSession();
        }
    }

    public function getUser()
    {
        $session = $this->getSession();

        if (isset($session['id'])) {

            if ($this->dataCache['user']) {
                return $this->dataCache['user'];
            }

            $user = User::findFirstById($session['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            if ($this->loggedStorage) {
                $this->loggedStorage->addSessionId($user->getId(), $this->getHashedSessionId());
            }

            return $this->dataCache['user'] = $user;
        }

        return false;
    }

    public function getSession()
    {
        return $this->session->get($this->sessionName);
    }

    public function updateSession($name, $value)
    {
        $session = $this->getSession();
        $session[$name] = $value;
        $this->session->set($this->sessionName, $session);
    }

    public function createSession(User $user = null)
    {
        if ($user === null) {
            $this->session->set($this->sessionName, [
                'role' => 'Unlogged'
            ]);
        } else {
            $this->session->set($this->sessionName, [
                'id' => $user->getId(),
                'role' => $user->role->getName()
            ]);

            if ($this->loggedStorage) {
                $this->loggedStorage->addSessionId($user->getId(), $this->getHashedSessionId());
            }
        }
    }

    public function logout()
    {
        $rememberMeService = Services::getService('User\RememberMe');
        $rememberMeService->delete();

        if ($this->loggedStorage) {
            $this->loggedStorage->deleteSessionId($this->getUser()->getId(), $this->getHashedSessionId());
        }

        $this->session->remove($this->sessionName);
    }

    public function getSessionId()
    {
        return $this->session->getId();
    }

    public function getHashedSessionId()
    {
        return $this->hashSessionId($this->getSessionId());
    }

    public function hashSessionId($sessionId)
    {
        if ($this->dataCache['hashedSessionId'][$sessionId]) {
            return $this->dataCache['hashedSessionId'][$sessionId];
        } else {
            return $this->dataCache['hashedSessionId'][$sessionId] = hash($this->getDI()->get('config')->auth->hashMethod, $sessionId);
        }
    }

}
