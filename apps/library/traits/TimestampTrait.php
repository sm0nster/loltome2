<?php

namespace Loltome\Traits;


trait TimestampTrait
{
    public function beforeValidationOnCreate()
    {
        if (property_exists($this, 'created_at')) {
            $this->setCreatedAt((new \DateTime()));
        }
    }

    public function beforeUpdate()
    {
        if (property_exists($this, 'updated_at')) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getDateTimeUTCString()
    {
        return self::dateTimeToUTCString((new \DateTime()));
    }

    public static function dateTimeToUTCString(\DateTime $dateTime)
    {
        return gmdate('Y-m-d H:i:s', strtotime($dateTime->format('Y-m-d H:i:s e')));
    }

    public static function stringToDateTime($string)
    {
        $time = strtotime($string);
        if(!$time){
            return false;
        }
        $newformat = date('Y-m-d', $time);
        return \DateTime::createFromFormat('Y-m-d', $newformat);
    }
}