<?php

namespace Loltome\Models\Entities;

use Loltome\Traits\TimestampTrait;

/**
 * Class ModelBase
 * @package Loltome\Models\Entities
 * @method static findFirstById(int $id)
 */
class ModelBase extends \Phalcon\Mvc\Model
{
    use TimestampTrait;

    /**
     * @return \DateTime
     */
    public function getLatestDate()
    {
        if ($this->updated_at != null) {
            return $this->getUpdatedAt();
        } else {
            return $this->getCreatedAt();
        }
    }

    public function getInternalRelated()
    {
        return $this->_related;
    }

    /**
     * @param $relatedColumnName
     * @param $relatedColumnValue
     * @return mixed
     */
    public static function countBelongingTo($relatedColumnName, $relatedColumnValue)
    {
        $results = self::findFirst([
            'columns' => ['count(*) as count'],
            'conditions' => $relatedColumnName . ' = :condition:',
            'bind' => ['condition' => $relatedColumnValue]
        ])->toArray();
        return $results['count'];
    }
}
