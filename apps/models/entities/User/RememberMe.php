<?php

namespace Loltome\Models\Entities\User;

use Loltome\Models\Entities\ModelBase;
use Phalcon\Forms\Element\Date;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class RememberMe extends ModelBase
{
    /**
     * @var integer
     */
    protected $id;
    /**
     * @var integer
     */
    protected $user_id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $user_agent;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RememberMe
     */
    public function setId(int $id): RememberMe
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return RememberMe
     */
    public function setUserId(int $user_id): RememberMe
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return RememberMe
     */
    public function setCode(string $code): RememberMe
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->user_agent;
    }

    /**
     * @param string $user_agent
     * @return RememberMe
     */
    public function setUserAgent(string $user_agent): RememberMe
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return (new \DateTime($this->created_at));
    }

    /**
     * @param string $created_at
     * @return RememberMe
     */
    public function setCreatedAt(\DateTime $created_at): User
    {
        $this->created_at = self::dateTimeToUTCString($created_at);
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updated_at == null ? null : (new \DateTime($this->updated_at));
    }

    /**
     * @param string $updated_at
     * @return RememberMe
     */
    public function setUpdatedAt(\DateTime $updated_at): User
    {
        $this->updated_at = self::dateTimeToUTCString($updated_at);
        return $this;
    }

    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'code',
            new Uniqueness([
                'model' => $this,
                'message' => 'Code must be uniquness'
            ])
        );

        return $this->validate($validator);
    }

    public function initialize()
    {
        $this->setSource('user_rememberme');
        $this->useDynamicUpdate(true);
        $this->keepSnapshots(true);

        $this->belongsTo('user_id', 'Loltome\Models\Entities\User\User', 'id', [
            'alias' => 'user',
            'foreignKey' => [
                'message' => 'The user doest noe exist'
            ]
        ]);
    }
}
