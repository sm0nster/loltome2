<?php

namespace Loltome\Models\Entities\User;

use Loltome\Models\Entities\ModelBase;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Role extends ModelBase
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $loggable;

    /**
     * @var string
     */
    protected $fullname;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Role
     */
    public function setId(int $id): Role
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Role
     */
    public function setName(string $name): Role
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoggable(): string
    {
        return $this->loggable;
    }

    /**
     * @param string $loggable
     * @return Role
     */
    public function setLoggable(string $loggable): Role
    {
        $this->loggable = $loggable;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullname(): string
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     * @return Role
     */
    public function setFullname(string $fullname): Role
    {
        $this->fullname = $fullname;
        return $this;
    }

    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'name',
            new Uniqueness([
                'model' => $this,
                'message' => 'Name role musi być unikalny.'
            ])
        );

        return $this->validate($validator);
    }



    public static function getTable()
    {
        return 'user_role';
    }

    public function initialize()
    {
        $this->useDynamicUpdate(true);
        $this->setSource(self::getSource());

        $this->hasMany('id', 'Loltome\Models\User\User', 'role_id', [
            'alias' => 'user',
            'reusable' => true,
            'foreignKey' => [
                'message' => 'Role nie może być usunięty, ponieważ ma przypisanych użytkowników.',
                'action' => Relation::NO_ACTION
            ]
        ]);
    }

}
