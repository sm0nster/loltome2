<?php

namespace Loltome\Models\Entities\User;

use Loltome\Models\Entities\ModelBase;
use Phalcon\Mvc\Model\Relation;

class User extends ModelBase
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $nickname;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return User
     */
    public function setNickname(string $nickname): User
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return (new \DateTime($this->created_at));
    }

    /**
     * @param string $created_at
     * @return User
     */
    public function setCreatedAt(\DateTime $created_at): User
    {
        $this->created_at = self::dateTimeToUTCString($created_at);
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updated_at == null ? null : (new \DateTime($this->updated_at));
    }

    /**
     * @param string $updated_at
     * @return User
     */
    public function setUpdatedAt(\DateTime $updated_at): User
    {
        $this->updated_at = self::dateTimeToUTCString($updated_at);
        return $this;
    }

    public static function getTable()
    {
        return 'user';
    }

    public function initialize()
    {
        $this->useDynamicUpdate(true);
        $this->setSource(self::getSource());

        $this->belongsTo('role_id', 'Loltome\Models\Entities\User\Role', 'id', [
            'alias' => 'role',
            'reusable' => true,
            'foreignKey' => [
                'allowNulls' => false,
                'message' => 'The role doest not exist'
            ]
        ]);

        $this->hasMany('id', 'Loltome\Models\Entities\User\RememberMe', 'user_id', [
            'alias' => 'remembersMe',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE
            ]
        ]);
    }

}
