<?php

namespace Loltome\Models\Repositories\Repository;

use Loltome\Models\Entities\User as EntityUser;

class UserRepository
{
    public function getLast()
    {
        return EntityUser::query()
            ->orderBy('datetime DESC')
            ->execute();
    }
}
