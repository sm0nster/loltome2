<?php
namespace Loltome\Models\Services\Service;

use Loltome\Models\Repositories\Repositories;

class UserService
{
    public function getLast()
    {
        return Repositories::getRepository('User')->getLast();
    }
}
