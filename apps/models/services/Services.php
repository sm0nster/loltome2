<?php
namespace Loltome\Models\Services;

use Loltome\Models\Services\Exceptions;

abstract class Services
{
    public static function getService($name)
    {
        $name .= 'Service';
        $className = "\\Modules\\Models\\Services\\Service\\{$name}";

        if (!class_exists($className)) {
            throw new Exceptions\InvalidServiceException("Class {$className} doesn't exists.");
        }

        return new $className();
    }
}
