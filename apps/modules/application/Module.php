<?php

namespace Modules\Modules\Application;

use Phalcon\DiInterface;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt;

class Module implements ModuleDefinitionInterface
{
    //Registered services and autoloaders for only this module
    public function registerAutoloaders(DiInterface $di = null)
    {
//        $loader = new Loader();
//
//        $loader->registerNamespaces(
//            [
//                'Modules\Modules\Application\Controllers' => __DIR__ . '/controllers/',
//            ]
//        );
//
//        $loader->register();
    }

    public function registerServices(DiInterface $di)
    {
        $di->set('view', function() use ($di) {
            $view = new View();

            $volt = new Volt($view, $di);

            $volt->setOptions(
                [
                    'compiledPath' => APP_PATH . $di->get('config')->volt->compiledPath,
                    'compiledExtension' => $di->get('config')->volt->extension,
                    'compiledSeparator' => $di->get('config')->volt->separator,
                    'stat' => (bool)$di->get('config')->volt->stat,
                    'compileAlways' => (bool)$di->get('config')->volt->compileAlways,
                ]
            );

            $view->disableLevel(
                [
                    View::LEVEL_LAYOUT      => true,
                    View::LEVEL_MAIN_LAYOUT => true
                ]
            );

            $view->setViewsDir(__DIR__ . '/views/');

            $view->registerEngines([
                '.volt' => $volt
            ]);

            return $view;
        });
    }
}
