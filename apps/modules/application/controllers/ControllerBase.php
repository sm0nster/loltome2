<?php
namespace Modules\Application\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $this->tag->setTitle('LOLTOME');

        if(APP_ENV == 'dev'){
            $this->tag->prependTitle('[DEV] ');
        }
    }
}
