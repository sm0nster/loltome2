<?php

namespace Modules\Application\Controllers;

/**
 * Class ErrorController
 * @package Modules\Application\Controllers
 *
 * @RoutePrefix("/")
 */
class ErrorController extends ControllerBase
{
    /**
     * @Route("show404",  methods={"GET"}, name="application.error.error")
     */
    public function show404Action()
    {

    }
}
