<?php
namespace Modules\Application\Controllers;

/**
 * Class IndexController
 * @package Modules\Application\Controllers
 *
 * @RoutePrefix("/")
 */
class IndexController extends ControllerBase
{
    /**
     * @Route("/",  methods={"GET"}, name="application.index.index")
     */
    public function indexAction()
    {
//        $this->response->redirect($this->url->get(['for' => 'application.landing.index']));
        echo '';
    }

    /**
     * @Route("index2",  methods={"GET"}, name="application.index.index2")
     */
    public function index2Action()
    {
//        $this->response->redirect($this->url->get(['for' => 'application.error.error']));
        echo '';
    }

    /**
     * @Route("index3",  methods={"GET"}, name="application.index.index3")
     */
    public function index3Action()
    {
//        $this->response->redirect($this->url->get(['for' => 'application.error.error']));
        echo '';
    }
}
