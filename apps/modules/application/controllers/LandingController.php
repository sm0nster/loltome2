<?php
namespace Modules\Application\Controllers;
use Modules\Application\Forms\LoginForm;

/**
 * Class LandingController
 * @package Modules\Application\Controllers
 *
 * @RoutePrefix("")
 */
class LandingController extends ControllerBase
{
    /**
     * @Route("/welcome",  methods={"GET", "POST"}, name="application.landing.index")
     */
    public function indexAction()
    {
        $this->view->form = new LoginForm();
    }

    /**
     * @Route("/register",  methods={"GET"}, name="application.landing.register")
     */
    public function registerAction()
    {
        $this->view->form = new LoginForm();
    }

}
