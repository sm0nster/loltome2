<?php
namespace Modules\Application\Forms;

use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;


class LoginForm extends Form
{
    public function initialize()
    {
        $this->setEntity($this);

        $email = new Email('email');
        $email->setLabel('Email');
        $email->setFilters(['xss', 'trim']);
        $email->addValidators([
            new PresenceOf(['message' => 'Email is required']),
        ]);
        $this->add($email);

        $password = new Password('password');
        $password->setLabel('Password');
        $password->addValidators([
            new PresenceOf(['message' => 'Password is required']),
        ]);
        $this->add($password);

        $rememberMe = new Check('remember', ['value' => '1']);
        $rememberMe->setLabel('Remember me');
        $rememberMe->setDefault(1);
        $this->add($rememberMe);

        $csrf = new Hidden('csrf', ['value' => $this->security->getSessionToken()]);
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'A validation error occurred. Please try again.'
        ]));
        $this->add($csrf);

        $submit = new Submit('submit');
        $submit->setDefault('Log in');
        $this->add($submit);


    }

    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}