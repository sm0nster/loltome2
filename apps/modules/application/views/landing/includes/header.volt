<div class="navbar navbar-default row">
    <div class="menu">
        <div class="container">
            <ul class="nav navbar-nav navbar-right">
                {{ form(
                    'action': url.get(['for': 'application.landing.index']),
                    'id': 'login-form',
                    'class': 'form-inline navbar-form navbar-right',
                    'novalidate': 'novalidate'
                ) }}
                {{ flashSession.output() }}
                    <div class="form-group form-group-sm">
                        {{ form.render('email', [
                            'id': 'login-email',
                            'class': 'form-control input-sm',
                            'placeholder': 'Email'
                        ]) }}
                    </div>
                    <div class="form-group">
                        {{ form.render('password', [
                            'id': 'login-password',
                            'class': 'form-control input-sm',
                            'placeholder': 'Password'
                        ]) }}
                    </div>
                    {{ form.label('remember') }}
                    {{ form.render('remember') }}
                    {{ form.render('csrf') }}
                    {{ form.render('submit', [
                        'class': 'btn btn-primary btn-sm'
                    ]) }}
                {{ end_form() }}
            </ul>
        </div>
    </div>
</div>