{% extends "layout-unlogged.volt" %}
{% block main %}
    <div class="container">
        <div class="jumbotron">
            <h1>Witaj na stronie IDTGA!</h1>
            <p>Jeśli masz konto możesz zalogować się w menu na górze, nastomiast jeśli nie posiadasz jeszcze konta w naszym
                serwisie możesz je założyć korzystając z przycisku poniżej</p>
            <p><a class="btn btn-success btn-lg" href="{{ url.get(['for' : 'application.landing.register']) }}" role="button">Dołącz do
                    IDTGA</a></p>
        </div>
    </div>
{% endblock %}
