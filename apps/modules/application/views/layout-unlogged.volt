<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8"/>
    {{ tag.getTitle() }}
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="{{ static_url('static/application/css/style.css') }}" type="text/css" media="screen"/>
<script src="{{ static_url('static/application/js/app.js') }}"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
      content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<base href="{{ url.get(['for' : 'application.index.index']) }}">
<!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
<!--[if IE]>
<link rel="shortcut icon" href=""><![endif]-->
<!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
<link rel="apple-touch-icon-precomposed" href="">
<!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
<link rel="icon" href="">

</head>
<body>
<div class="off-canvas-wrap">
    <div class="inner-wrap">
        <div class="welcome">
            <div class="login">
                {% include 'landing/includes/header.volt' %}
            </div>
            <div class="content">
                {% block main %}{% endblock %}
            </div>
        </div>
    </div>
</div>
{% include 'footer.volt' %}
</body>
</html>