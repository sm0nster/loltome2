var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

var staticPath = '../public/static/';
//expanded or compressed
var cssCompressed = {outputStyle: 'expanded'};
var jsCompressed = false;

//tasks
gulp.task('_build-prod-files', buildProdFiles);

gulp.task('build-sass', buildSass);
gulp.task('watch-sass', watchSass);

gulp.task('build-js', buildJs);
gulp.task('watch-js', watchJs);


//functions
function buildProdFiles() {
    cssCompressed = {outputStyle: 'compressed'};
    jsCompressed = true;
    console.log('build CSS files');
    gulp.start('build-sass');
    console.log('build JS files');
    gulp.start('build-js');
    cssCompressed = {outputStyle: 'expanded'};
    jsCompressed = false;
}

function buildSass() {
    gulp.src('assets/application/scss/main.scss')
        .pipe(sass({includePaths: ['bower_components/bootstrap-sass/assets/stylesheets']}))
        .pipe(sass(cssCompressed).on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(staticPath + '/application/css/'))
}

function watchSass() {
    gulp.watch('assets/application/scss/**/*.scss', ['build-sass']);
}

function buildJs() {
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap-sass/javascripts/bootstrap.min.js',
        'assets/application/js/*.js',
        'assets/application/js/**/*.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(gulpif(jsCompressed, uglify({
            mangle: false
        })))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(staticPath + '/application/js/'));
}

function watchJs() {
    gulp.watch('assets/application/js/**/*.js', ['build-js']);
}