<?php

use Phalcon\Mvc\Application;

try {
    define('APP_PATH', realpath('..') . '/apps/');

    require APP_PATH . 'config/definitions.php';
    require APP_PATH . 'config/loader.php';
    require APP_PATH . 'config/services.php';

    $application = new Application($di);

    require APP_PATH . 'config/modules.php';

    echo $application->handle()->getContent();
} catch (Phalcon\Exception $e) {
    echo '<b>Message:</b> ' . $e->getMessage() . '<br>';
    echo '<b>Code:</b> ' . $e->getCode() . '<br>';
    echo '<b>File:</b> ' . $e->getFile() . '<br>';
    echo '<b>Line:</b> ' . $e->getLine() . '<br>';
} catch (PDOException $e) {
    echo $e->getMessage();
}